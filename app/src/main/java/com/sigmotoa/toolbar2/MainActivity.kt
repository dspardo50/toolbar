package com.sigmotoa.toolbar2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.materialToolbar))
    }

 override fun onCreateOptionsMenu(menu: Menu?): Boolean {
       menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }
    private fun setSupportActionBar(materialToolbar: Int) {
        TODO("Not yet implemented")
    }

    override fun onOptionsItemSelected(item: MenuItem)= when(item.itemId){
        R.id.home ->{
            startActivity(Intent(this, Ayuda::class.java))
            msg("Go to Home")
            true

        }
        R.id.music ->{
            msg("Go to Music")
            true
        }
        R.id.call ->{
            msg("Go to Call")
            true
        }
        R.id.icecream ->
        {
            msg("Go to IceCream")
            true
        }
        else -> super.onOptionsItemSelected(item)

    }

    fun msg (msg:String)
    {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}








